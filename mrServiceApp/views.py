from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from .forms import CreateUserForm, formPost
from .models import PostServicio


def register_user(request):
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            # print('paso')
            form.save()
            return redirect('home')
    else:
        form = CreateUserForm()
    context = {'form': form}
    return render(request, 'signup.html', context)


def login_user(request):
    message = ''
    if request.method == 'POST':
        user_name = request.POST['user_name']
        password = request.POST['password']
        user = authenticate(request, username=user_name, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            message = 'Username or password incorrect'

    context = {'message': message}
    return render(request, 'login.html', context)


def hola(request):
    return render(request, 'home.html')


class postServicio(CreateView):
    model = PostServicio
    form_class = formPost
    template_name = 'formularioPost.html'
    context_object_name = 'postear'
    success_url = ('/')


def Electricidad(request):
    data = PostServicio.objects.filter(servicio='Electricidad')
    return render(request, 'electricidad.html', {'data': data})


def EliminarElectricidad(request, id):
    post = PostServicio.objects.get(id=id)
    post.delete()
    post = PostServicio.objects.all()
    return render(request, 'electricidad.html', {'post_delete': post})


def pre_eliminar(request, id):
    return render(request, 'eliminar.html', {'id': id})


class EditView(UpdateView):
    model = PostServicio
    form_class = formPost
    pk_url_kwarg = 'id'
    template_name = 'editarElectricidad.html'
    success_url = ('/')


class DetailedView(DetailView):
    model = PostServicio
    template_name = 'detalle_prestador.html'
    context_object_name = 'post'


'''-------------------------------------------------------------'''


def ListAcabados(request):
    data = PostServicio.objects.filter(servicio='Acabados')
    return render(request, 'ListAcabados.html', {'data': data})


def ListPlomeria(request):
    data = PostServicio.objects.filter(servicio='Plomeria')
    return render(request, 'plomeria.html', {'data': data})


def ListCarpinteria(request):
    data = PostServicio.objects.filter(servicio='Carpinteria')
    return render(request, 'carpinteria.html', {'data': data})

# Create your views here.

from django.contrib import admin
from django.urls import path
from . import views
from .views import register_user, login_user, postServicio, EditView, ListAcabados, ListPlomeria, DetailedView

urlpatterns = [
    path('', views.hola, name='home'),
    path('registrar_usuario/', register_user, name='register_user'),
    path('loguear_usuario/', login_user, name='login_user'),
    path('postServicio/', postServicio.as_view(), name='post_servicio'),

    path('electricidad/', views.Electricidad, name='electricidad'),
    path('deleteElectric/<int:id>', views.EliminarElectricidad, name='deleteElectric'),
    path('pre_eliminar/<int:id>', views.pre_eliminar, name='pre_eliminar'),
    path('edit_post/<int:id>', EditView.as_view(), name='edit_post'),

    path('Listacabados/', views.ListAcabados, name='Listacabados'),
    path('ListPlomeria/', views.ListPlomeria, name='ListPlomeria'),
    path('Listcarpinteria/', views.ListCarpinteria, name='Listcarpinteria'),
    path('detail_view/<int:pk>',DetailedView.as_view(), name='detail_view'),







]
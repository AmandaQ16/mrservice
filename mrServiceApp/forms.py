from django import forms
from mrServiceApp.models import Usuario, TipoUsuario, PostServicio
from django.contrib.auth.forms import User
from django.contrib.auth.forms import UserCreationForm


class CreateUserForm(UserCreationForm):
    user_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    option = forms.ModelChoiceField(queryset=TipoUsuario.objects.all(),
                                    widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = ['user_name', 'first_name', 'last_name', 'email', 'password1', 'password2', 'option']

    def save(self, commit=True):
        new_user = User.objects.create_user(
            username=self.cleaned_data["user_name"],
            password=self.cleaned_data['password1']
        )
        Usuario.objects.create(
            nombre=self.cleaned_data["first_name"],
            apellido=self.cleaned_data["last_name"],
            email=self.cleaned_data["email"],
            tipo_usuario=self.cleaned_data["option"],
            user=new_user
        )


class formPost(forms.ModelForm):
    tipo_servicio = (
        ('Electricidad', 'Electricidad'),
        ('Carpinteria', 'Carpinteria'),
        ('Plomeria', 'Plomeria'),
        ('Acabados', 'Acabados')
    )
    servicio = forms.ChoiceField(required=True, choices=tipo_servicio)
    descripcion = forms.Textarea()
    id_usuario = forms.ModelChoiceField(queryset=Usuario.objects.all())
    num_telefono = forms.IntegerField(widget=forms.NumberInput())
    class Meta:
        model = PostServicio
        fields = ['servicio', 'descripcion', 'id_usuario','num_telefono']
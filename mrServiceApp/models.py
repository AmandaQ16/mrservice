from xxlimited import Null

# Create your models here.
from django.contrib.auth.models import User
from django.db import models


class TipoUsuario(models.Model):
    tipo_usuario = models.CharField(max_length=25, blank=False, null=False)

    def __str__(self):
        return self.tipo_usuario


class Usuario(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    tipo_usuario = models.ForeignKey(TipoUsuario, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre


class PostServicio(models.Model):
    tipo_servicio = (
        ('Electricidad', 'Electricidad'),
        ('Carpinteria', 'Carpinteria'),
        ('Plomeria', 'Plomeria'),
        ('Acabados', 'Acabados')
    )
    servicio = models.CharField(max_length=20, choices=tipo_servicio)
    descripcion = models.TextField()
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    num_telefono = models.IntegerField()

    def __str__(self):
        return self.servicio





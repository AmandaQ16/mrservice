from django.contrib import admin
from .models import TipoUsuario,  PostServicio, Usuario

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre', 'apellido', 'email', 'tipo_usuario')
    search_fields = ('id', 'nombre')

@admin.register(TipoUsuario)
class TipoUsuarioAdmin(admin.ModelAdmin):
    list_display = ('id', 'tipo_usuario')

@admin.register(PostServicio)
class CategoriaServicioAdmin(admin.ModelAdmin):
    list_display = ('id', 'servicio', 'descripcion', 'id_usuario')


# Register your models here.
